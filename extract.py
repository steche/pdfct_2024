import numpy as np
import matplotlib.pyplot as plt
import h5py


with h5py.File('fbprec_glass50_gtzD40_gtzCM08_0002_scan0001_pilatus.h5','r') as h:
    tth = h['tth'][()]
    recon = h['reconstruction'][()]
print(f'2theta from {tth[0]:.1f} to {tth[-1]:.1f}')
print(f'reconstruction shape: {recon.shape}')


p1 = (53, 15)

plt.imshow(np.where(recon[p1[0],:,:] > p1[1], recon[p1[0],:,:], np.nan))
plt.show()
